using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<PlayerController>().CurrentHP = 0;
        }
        if (collision.TryGetComponent<EnemyAI>(out var enemyAI))
        {
            enemyAI.HP = 0;
        }
        if (collision.CompareTag("RollingRock"))
        {
            collision.GetComponent<Destroy>().AutoDestroy();
        }
    }
}
