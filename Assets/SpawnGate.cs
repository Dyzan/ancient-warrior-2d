using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGate : MonoBehaviour
{
    public GameObject[] gate;

    private void Start()
    {
        for (int i = 0; i < gate.Length; i++)
        {
            gate[i].SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            for (int i = 0; i < gate.Length; i++)
            {
                gate[i].SetActive(true);
            }
        }  
    }
}
