using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBEam : MonoBehaviour
{
    float getdame = 0;
    public int dame;
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Get Player");
            if (collision.TryGetComponent<PlayerController>(out var pl))
            {
               
                    Debug.Log("Get dame");
                    pl.CurrentHP -= dame;
                    getdame = Time.time;
               GetComponent<Collider2D>().enabled = false;
            }
        }
    }
    private void Update()
    {
        if (Time.time - getdame > 1f)
        {
            GetComponent<Collider2D>().enabled = true;
        }
    }
}
