using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandle : MonoBehaviour
{

    public int coinExtra;
    [SerializeField] private GameObject[] Enemys;
    private int enemySize;
    
    private void Start()
    {
        FindObjectOfType<PlayerController>().OnHPChange.AddListener(HandleHpChange);
        enemySize= Enemys.Length;
        foreach (var enemy in Enemys)
        {
            enemy.GetComponent<EnemyAI>()?.OnDead.AddListener(OnEnemyDead);
        }
    }

    public void OnEnemyDead(int hp)
    {
        enemySize--;
        Debug.Log(enemySize);
        if(enemySize == 0)
        {
            GameManager.instanceProfile.WinGame();
            GameManager.instanceProfile.Coin += coinExtra;
            GameManager.instanceProfile.PauseGame();
        }
    }

    private void HandleHpChange(int curren, int max)
    {
        if (curren > 0) return;
        //lose 
        GameManager.instanceProfile.LoseGame();
      GameManager.instanceProfile.PauseGame();
    }
}
