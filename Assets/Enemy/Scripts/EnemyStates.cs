using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public enum STATE
{
    ENTER,
    UPDATE,
    EXIT,
}
namespace EnemyState
{
    public enum ENEMYSTATE
    {
        IDLE,
        MOVE,
        CHASE,
        MELEEATTACK,
        RANGERATTACK,
        DIE,
        GETHIT,

    }
    public class EnemyStates
    {
        protected static float attackDelay; //Delaytime for each attack
        public float resetRamdom;
        protected Animator anim;
        protected Rigidbody2D rig2d;
        protected EnemyData data;

        protected STATE state;
        public ENEMYSTATE stateName;
        protected EnemyStates nextState;

        protected UnityEvent OnAnimationEnd;
        public EnemyStates(Animator anim, Rigidbody2D rig2d, EnemyData data)
        {
            state = STATE.ENTER;
            this.anim = anim;
            this.rig2d = rig2d;
            this.data = data;

        }
        public virtual void EnterState()
        {
            state = STATE.UPDATE;
        }
        public virtual void UpdateState()
        {

            state = STATE.UPDATE;
        }

        protected bool CheckChaseChange()
        {
            if (data.CanChase)
            {
                var colliders = Physics2D.OverlapCircleAll(rig2d.position, data.ChaseRange);

                if (colliders != null)
                {
                    foreach (var col in colliders)
                    {
                        if (col.CompareTag("Player"))
                        {
                            nextState = new ChaseState(anim, rig2d, data, col.transform);
                            ExitState();
                            return true;
                        }

                    }

                }

            }
            return false;
        }

        protected bool CheckRangerAttackState()
        {
            if (data.IsRanger)
            {
                var colliders = Physics2D.OverlapCircleAll(rig2d.position, data.RangerAttackRange);

                if (colliders != null)
                {
                    foreach (var col in colliders)
                    {
                        if (col.CompareTag("Player"))
                        {
                            nextState = new RangerAttackState(anim, rig2d, data, col.transform);
                            ExitState();
                            return true;
                        }

                    }
                }
            }
            return false;
        }
        public virtual void ExitState()
        {
            state = STATE.EXIT;
        }
        public EnemyStates Process()
        {
            if (state == STATE.EXIT)
            {
                ExitState();
                return nextState;
            }
            else if (state == STATE.ENTER)
            {
                EnterState();
            }
            else if (state == STATE.UPDATE)
            {
                UpdateState();
            }
            return this;
        }
        public void AnimationResetAllTrigger()
        {
            foreach (var param in anim.parameters)
            {
                if (param.type == AnimatorControllerParameterType.Trigger)
                {
                    anim.ResetTrigger(param.name);
                }
            }
        }
        public void CallEndAnimationEvent() => OnAnimationEnd.Invoke();
    }
    public class IdleState : EnemyStates
    {
        public IdleState(Animator anim, Rigidbody2D rig2d, EnemyData data) : base(anim, rig2d, data)
        {

            stateName = ENEMYSTATE.IDLE;
        }
        public override void EnterState()
        {
            anim.SetBool("Idle", true);
            base.EnterState();
        }
        public override void UpdateState()
        {
            base.UpdateState();
            if (CheckChaseChange()) return;
            if (CheckRangerAttackState()) return;
       
            if (Time.time - resetRamdom > 2f)
            {

                var ramdomFloat = Random.Range(1f, 10f);

                if (ramdomFloat > 3f)
                {
                    nextState = new MoveState(anim, rig2d, data);
                    state = STATE.EXIT;
                }
                anim.SetFloat("Random", ramdomFloat);
                resetRamdom = Time.time;
            }

        }
        public override void ExitState()
        {
            anim.SetBool("Idle", false);
            base.ExitState();
        }

    }

    public class MoveState : EnemyStates
    {
        private Transform groundCheck;
        public MoveState(Animator anim, Rigidbody2D rig2d, EnemyData data) : base(anim, rig2d, data)
        {
            groundCheck = rig2d.GetComponentsInChildren<Transform>()[2];
            stateName = ENEMYSTATE.MOVE;
        }
        public override void EnterState()
        {
            anim.SetBool("Move", true);
            base.EnterState();
        }
        public override void UpdateState()
        {
            if (CheckRangerAttackState()) return;
            if (CheckChaseChange()) return;
            if (Physics2D.OverlapCircleAll(groundCheck.position, 1f, LayerMask.NameToLayer("Ground")) == null)
            {
                nextState = new IdleState(anim, rig2d, data);
                state = STATE.EXIT;
                return;
            }
            base.UpdateState();
            if (Time.time - resetRamdom > 2f)
            {
                float randomVelocity = 0;
                var ramdomFloat = Random.Range(1f, 10f);
                if (ramdomFloat < 3f)
                {
                    randomVelocity = data.Speed;
                    state = STATE.UPDATE;
                }
                else if (ramdomFloat < 6f)
                {
                    randomVelocity = -data.Speed;
                    state = STATE.UPDATE;
                }
                else
                {

                    nextState = new IdleState(anim, rig2d, data);
                    state = STATE.EXIT;

                }
                resetRamdom = Time.time;
                rig2d.velocity = new Vector2(randomVelocity, rig2d.velocity.y);
                if (rig2d.velocity.x > 0)
                {
                    rig2d.transform.localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    rig2d.transform.localScale = new Vector3(-1, 1, 1);
                }
            }


        }
        public override void ExitState()
        {
            anim.SetBool("Move", false);
            base.ExitState();
        }
    }
    public class ChaseState : EnemyStates
    {
        private Transform groundCheck;
        private Transform target;
        Vector2 offset;
        public ChaseState(Animator anim, Rigidbody2D rig2d, EnemyData data, Transform target) : base(anim, rig2d, data)
        {
            groundCheck = rig2d.GetComponentsInChildren<Transform>()[2];
            this.stateName = ENEMYSTATE.CHASE;
            this.target = target;
        }

        public override void EnterState()
        {
            anim.SetBool("Chase", true);
            base.EnterState();
        }
        public override void UpdateState()
        {
            offset = rig2d.position - new Vector2(target.position.x, target.position.y);
    
            if (CheckAttackState())
            {
                state = STATE.EXIT;
                return;
            }
            if(Physics2D.OverlapCircleAll(groundCheck.position,1f,LayerMask.NameToLayer("Ground")) == null)
            {
                nextState = new IdleState(anim, rig2d, data);
                state = STATE.EXIT;
                return;
            }

            if (offset.magnitude > data.ChaseRange)
            {
                nextState = new IdleState(anim, rig2d, data);
                state = STATE.EXIT;
                return;
            }
            float randomVelocity;
            var number = rig2d.position.x - target.position.x;
            if (number < 0)
            {
                randomVelocity = data.Speed * 2;
                state = STATE.UPDATE;
            }
            else
            {
                randomVelocity = -data.Speed * 2;
                state = STATE.UPDATE;
            }
            if(offset.magnitude > data.AttackRange)
            rig2d.velocity = new Vector2(randomVelocity, rig2d.velocity.y);
            if (rig2d.velocity.x > 0)
            {
                rig2d.transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                rig2d.transform.localScale = new Vector3(-1, 1, 1);
            }
            base.UpdateState();

        }

        private bool CheckAttackState()
        {
            if (data.CanAttack && offset.magnitude < data.AttackRange)
            {
                if (Time.time - attackDelay > data.AttackDelayTime)
                {
                    nextState = new AttackState(anim, rig2d, data);

                    return true;
                }
            }


            return false;


        }

        public override void ExitState()
        {
            anim.SetBool("Chase", false);
            base.ExitState();
        }
    }
    public class AttackState : EnemyStates
    {
        public AttackState(Animator anim, Rigidbody2D rig2d, EnemyData data) : base(anim, rig2d, data)
        {
            this.stateName = ENEMYSTATE.MELEEATTACK;
            rig2d.GetComponent<EnemyAI>()?.OnAnimEnd.AddListener(HandleEndAniamtionEvent);

        }

        public override void EnterState()
        {
            anim.SetBool("Attack", true);
            rig2d.velocity = new Vector2(0, 0);
            base.EnterState();
        }
        public override void ExitState()
        {
            attackDelay = Time.time;
            nextState = new IdleState(anim, rig2d, data);
            anim.SetBool("Attack", false);
            base.ExitState();
        }
        protected void HandleEndAniamtionEvent()
        {
            ExitState();
        }
    }
    public class RangerAttackState : EnemyStates
    {
      protected  Transform target;
        public RangerAttackState(Animator anim, Rigidbody2D rig2d, EnemyData data,Transform target) : base(anim, rig2d, data)
        {
            this.target = target;
            stateName = ENEMYSTATE.RANGERATTACK;
            rig2d.GetComponent<EnemyAI>()?.OnAnimEnd.AddListener(HandleEndAniamtionEvent);
        }

        private void HandleEndAniamtionEvent()
        {
            ExitState();
        }

        public override void EnterState()
        {
           var offset = rig2d.position.x - target.position.x;
            if (offset < 0)
            {
                rig2d.transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                rig2d.transform.localScale = new Vector3(-1, 1, 1);
            }
            anim.SetBool("Throw", true);
            base.EnterState();
        }
        public override void ExitState()
        {
            nextState = new IdleState(anim, rig2d, data);
            anim.SetBool("Throw", false);
            base.ExitState();
        }
    }
    public class GetHitState : EnemyStates
    {
        EnemyStates lastState;
        public GetHitState(Animator anim, Rigidbody2D rig2d, EnemyData data,EnemyStates lastState) : base(anim, rig2d, data)
        {
          this.lastState = lastState;
            stateName = ENEMYSTATE.GETHIT;
        }
        public override void EnterState()
        {
            rig2d.GetComponent<EnemyAI>()?.OnAnimEnd.AddListener(HandleEndAniamtionEvent);
            var parameters = anim.parameters;
            foreach (var parameter in parameters)
            {
                if (parameter.type == AnimatorControllerParameterType.Bool)
                {
                    anim.SetBool(parameter.name, false);
                }
            }
            anim.SetBool("GetHit", true);
           
            rig2d.velocity = Vector2.zero;
            base.EnterState();
        }

        private void HandleEndAniamtionEvent()
        {
            nextState = lastState;
            state = STATE.EXIT;
        }

        public override void ExitState()
        {
            anim.SetBool("GetHit", false);
            base.ExitState();
        }
   
    }
    public class DeadState : EnemyStates
    {
        public DeadState(Animator anim, Rigidbody2D rig2d, EnemyData data) : base(anim, rig2d, data)
        {
            stateName = ENEMYSTATE.DIE;
        }
        public override void EnterState()
        {
            anim.SetBool("Dead", true);
            rig2d.velocity = Vector2.zero;
            rig2d.GetComponent<CapsuleCollider2D>().enabled = false;
            rig2d.GetComponent<EnemyAI>()?.OnAnimEnd.AddListener(HandleEndAniamtionEvent);
        }

        private void HandleEndAniamtionEvent()
        {
            MonoBehaviour.Destroy(rig2d.gameObject);
        }


    }
}



