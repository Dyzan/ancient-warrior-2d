using EnemyState;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyAI : MonoBehaviour
{
    private bool isActive = false;
    [SerializeField] public EnemyData data;
    [SerializeField] protected Animator anim;
    [SerializeField] protected Rigidbody2D rig2d;
    private EnemyStates currentState;
    public Transform attackPoint;
    public UnityEvent OnAnimEnd;
    public UnityEvent<int> OnHpChange, OnDead;
    private int hp;
    public int HP
    {
        get => hp;
        set
        {
            hp = value;
            OnHpChange.Invoke(value);
            if (value <= 0)
            {
                OnDead.Invoke(value);
            }
        }
    }

    private void OnValidate()
    {
        anim = GetComponent<Animator>();
        rig2d = GetComponent<Rigidbody2D>();
        if (gameObject.transform.childCount > 0) attackPoint = GetComponentsInChildren<Transform>()[1];

    }
    private void Awake()
    {
        currentState = new IdleState(anim, rig2d, data);
        hp = data.MaxHP;
        data.UI.GetComponent<EnemyHPUI>().enemy = this.transform;
        Instantiate(data.UI);
        OnDead.AddListener(Dead);
    }
    private void Update()
    {
        if(isActive)
        {
            currentState = currentState.Process();
        }
    }
    public void CallAnimEnd()
    {
        OnAnimEnd.Invoke();
    }
    public void Dead(int a)
    {
        currentState = new DeadState(anim, rig2d, data);
        DropItems();
    }

    private void DropItems()
    {
        var dropList = data.DropItemList;
        for (int i = 0; i < dropList.Length; i++)
        {
            for (int j = 0; j < dropList[i].quantity; j++)
            {
                var x = UnityEngine.Random.Range(-2, 2);
                var item = Instantiate(dropList[i].data.Item, new Vector2(this.transform.position.x + x, this.transform.position.y + 1f), Quaternion.identity);
                var audio = item.GetComponent<AudioSource>();
                audio.clip = dropList[i].data.PickUpSFX; 
                audio.Play();
            }

        }
    }
    public void GetHit(int hit)
    {
        HP -= hit;
        currentState = new GetHitState(anim, rig2d, data, currentState);
    }
    public void DealDame()
    {
        var colliders = Physics2D.OverlapCircleAll(attackPoint.position, data.AttackRadius);
        foreach (var collider in colliders)
        {
            if (collider.CompareTag("Player"))
            {
                if (collider.TryGetComponent<PlayerController>(out var playerController))
                {
                    playerController.CurrentHP -= data.Dame;
                    playerController.GetHurt();
                    if (playerController.CurrentHP <= 0)
                        playerController.Die();
                }
            }
        }
    }
    public void Throwing()
    {
        if (!data.IsRanger) return;
        var item = Instantiate(data.RangerAttackItem, attackPoint.transform.position, Quaternion.identity);
        item.transform.localScale = this.transform.localScale;
    }
    private void OnBecameVisible()
    {
        isActive = true;
    }
}
