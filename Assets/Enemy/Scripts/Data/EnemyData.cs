using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class EnemyData : ScriptableObject
{
    [field: SerializeField] public bool IsMelee { set; get; }
    [field: SerializeField] public bool IsRanger { set; get; }
    [field: SerializeField] public bool CanAttack { set; get; }
    [field: SerializeField] public float AttackRange { set; get; }
    [field: SerializeField] public float AttackRadius { set; get; }
    [field: SerializeField] public float AttackDelayTime { set; get; }
    [field: SerializeField] public float RangerAttackRange { set; get; }
    [field: SerializeField] public float RangerAttackDelayTime { set; get; }
    [field: SerializeField] public bool CanChase { set; get; }
    [field: SerializeField] public int ChaseRange { set; get; }
    [field: SerializeField] public int MaxHP { set; get; }
    [field: SerializeField] public int Dame { set; get; }
    [field: SerializeField] public float Speed { set; get; }
    [field: SerializeField] public GameObject RangerAttackItem { set; get; }
    [field: SerializeField] public GameObject AttackEffect { set; get; }
    [field: SerializeField] public GameObject DeadEffect { set; get; }
    [field: SerializeField] public GameObject UI { set; get; }
    [field: SerializeField] public DropList[] DropItemList { set; get; }
}
[System.Serializable]
public class DropList
{
    public DropItemData data;
    public int quantity;
}