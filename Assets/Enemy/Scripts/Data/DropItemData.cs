using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemData : ScriptableObject
{
    [field: SerializeField] public GameObject Item { get; private set; }
    [field: SerializeField] public AudioClip PickUpSFX { get; private set; }
    [field: SerializeField] public AudioClip DropSFX { get; private set; }
    public virtual void OnPickUp()
    {

    }
}
