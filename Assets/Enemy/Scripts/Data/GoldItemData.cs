using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="EnemyDrop/Coin")]
public class GoldItemData : DropItemData
{

    public override void OnPickUp()
    {
        GameManager.instanceProfile.Coin++;
    }
}
