using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawmer : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject EnemyUI;
    public Transform[] place;
    float lastspawm;

    private void Update()
    {
       var index =  Random.Range(0, place.Length);
       int  indexI = Mathf.RoundToInt(index);
        if(Time.time - lastspawm>2f)
        {
            InitEnemy(place[indexI].position);
            lastspawm = Time.time;
        }
       
    }
    public void InitEnemy(Vector3 location)
    {
        Instantiate(Enemy, location, Quaternion.identity);
        EnemyUI.GetComponent<EnemyHPUI>().enemy = Enemy.transform;
        Instantiate(EnemyUI);
    }

}
