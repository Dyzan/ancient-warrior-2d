using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamePopup : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float destroyTime;
    private float enableTime;
    private void Start()
    {

        enableTime = Time.time;
    }
    private void Update()
    {
        transform.position += Vector3.up * speed;
        if(Time.time - enableTime > destroyTime)
        {
            Destroy(gameObject);
        }
    }
}
