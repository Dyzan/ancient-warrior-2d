using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyThrowingItem : MonoBehaviour
{
    public float speed;
    public int dame;
    private void Update()
    {
        if(transform.localScale.x >0)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
       
    }
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player")|| collision.CompareTag("Ground"))
        {
            if(collision.TryGetComponent<PlayerController>(out var controlelr))
            {
                controlelr.CurrentHP -= dame;
             
            }
            Destroy(gameObject);
        }
    }
}
