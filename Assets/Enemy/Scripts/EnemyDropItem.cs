using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDropItem : MonoBehaviour
{
    [SerializeField] private DropItemData data;
    private Transform target;
    bool isFly = false;
    private void Start()
    {
        target = FindObjectOfType<PlayerController>().transform;
        StartCoroutine(nameof(AutoPickUp));
    }
    private void Update()
    {
        if (!isFly) return;
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StopAllCoroutines();
            StartCoroutine(nameof(PickUpEffect));
            data.OnPickUp();
        }
        if(collision.CompareTag("Ground"))
        {
            var rig = GetComponent<Rigidbody2D>();
            rig.gravityScale = 0;
            rig.velocity = Vector2.zero;
        }
    }
 
    IEnumerator PickUpEffect()
    {
        GetComponent<Collider2D>().enabled = false;
        var audio = GetComponent<AudioSource>();
        audio.clip = data.PickUpSFX;
        audio.Play();
        yield return new  WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
    IEnumerator AutoPickUp()
    {
        yield return new WaitForSeconds(1f);
        isFly = true;
    }
}
