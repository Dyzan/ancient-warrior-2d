using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boss : MonoBehaviour
{
    enum BossMove
    {
        Moving,
        GotoPlace,
        TurnLeft,
        TurnRight,
        Skill,
    }
    bool isDrop;
    public int timeMove, timeSkill;
    [SerializeField] Animator anim;
    [SerializeField] Rigidbody2D rig2D;
    [SerializeField] GameObject dropOJ;
    [SerializeField] GameObject lazerBeam;
    [SerializeField] Transform leftPos, rightPos;
    private Transform targetPlace;
    [SerializeField] Transform[] PathWay;
    [SerializeField] int speed;
    float skilltime;
    float movetime;
    int currentway;
     BossMove bossMove;
    int maxHp = 500;
    int hp;
    public int HP
    {
        get { return hp; }
        set { hp = value;
            Debug.Log(value);
            OnHp(value);
        }
    }
   
    UnityEvent<int> OnHPChange;
    void OnHp(int x)
    {
        if (x == 0)
        { Destroy(gameObject);
            FindObjectOfType<MapHandle>().OnEnemyDead(x);
        }
    }
    private void Start()
    {
        HP = maxHp;
        StartCoroutine(Drop());
        bossMove = BossMove.Moving;
        movetime = 0;
       
    }
    public void Update()
    {
      
        switch (bossMove)
        {
            case BossMove.Moving:
                Moving();
                break;
            case BossMove.GotoPlace:
                GotoPlace();
                break;
            case BossMove.TurnLeft:
                break;
            case BossMove.TurnRight:
                break;
            case BossMove.Skill:
                Skilling();
                break;
        }

    }
    IEnumerator Drop()
    {

        do {
            yield return new WaitForSeconds(2f);
            Instantiate(dropOJ, transform.position, Quaternion.identity);
        
        }while (isDrop);
    }
    private void Skilling()
    {
       if(Time.time - skilltime >timeSkill)
        {
            bossMove = BossMove.Moving;
            anim.SetBool("IsSkill", false);
            anim.SetBool("IsMove", true);
           
            lazerBeam.SetActive(false);
            transform.localScale = new Vector3(1, 1, 1);
            movetime = Time.time;
            isDrop = true;
            StartCoroutine(Drop());
        }
        else
        {
            lazerBeam.SetActive(true);
        }
    }

    private void GotoPlace()
    {

        if (Vector3.Magnitude(transform.position - targetPlace.position) < 1f)
        {
            anim.SetBool("IsMove",false);
            if(targetPlace==leftPos)
            {
                anim.SetBool("TurnRight", true);
            }else
            {
                anim.SetBool("TurnLeft", true);
            }
          
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPlace.position, speed * Time.deltaTime);
        }

    }

    private void Moving()
    {
        if(Time.time - movetime> timeMove)
        {
            bossMove = BossMove.GotoPlace;
            isDrop = false;
            int i = UnityEngine.Random.Range(0, 2);
            if (i == 0)
            {
                targetPlace = leftPos;
            }
            else
            {
                targetPlace = rightPos;
            }
        }
        if(currentway>=PathWay.Length)
        {
            currentway = 0;
        }
        if (Vector3.Magnitude(transform.position - PathWay[currentway].position) < 1f)
        {
            currentway++;
        }else
        {
            transform.position = Vector3.MoveTowards(transform.position, PathWay[currentway].position, speed * Time.deltaTime);
        }

    }
    public void LeftSkill()
    {
        Debug.Log("skill");
        bossMove = BossMove.Skill;
        skilltime = Time.time;
        transform.localScale = new Vector3(1, 1, 1);
        anim.SetBool("TurnLeft", false);
        anim.SetBool("TurnRight", false);
        anim.SetBool("IsSkill", true);
    }
    public void RightSkill()
    {
        Debug.Log("skill");
        bossMove = BossMove.Skill;
        skilltime = Time.time;
        transform.localScale = new Vector3(-1, 1, 1);
        anim.SetBool("TurnLeft", false);
        anim.SetBool("TurnRight", false);
        anim.SetBool("IsSkill", true);
    }
}
