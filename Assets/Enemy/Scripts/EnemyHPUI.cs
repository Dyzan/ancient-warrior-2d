using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyHPUI : MonoBehaviour
{
    [SerializeField] public Transform enemy;
    [SerializeField] private Image border;
    [SerializeField] private Image fill;
    [SerializeField] private GameObject textMP;
    private EnemyAI enemyAI;
    private int currenthp;
    private void Start()
    {
        if (enemy.TryGetComponent<EnemyAI>(out enemyAI))
        {
            currenthp = enemyAI.data.MaxHP;
            enemyAI.OnHpChange.AddListener(HandleHpChange);
            enemyAI.OnDead.AddListener(HandleEnemyDead);
        }
     
        
    }


    private void LateUpdate()
    {
        transform.position = enemy.position;
    }
    public void HandleHpChange(int hp)
    {
        StopCoroutine(nameof(DisableHPbar));
        border.enabled = true;
        fill.enabled = true;
        if(enemyAI!= null)
        {
            fill.fillAmount = hp * 1f / 1000;
        }
        else
        {
            fill.fillAmount = hp * 1f / enemyAI.data.MaxHP;
        }
     
        var popup = Instantiate(textMP, this.transform.position + Vector3.up * 2, Quaternion.identity);
        var textMPGUI = popup.GetComponentInChildren<TextMeshProUGUI>();
        textMPGUI.text = "-" + (currenthp - hp);
        currenthp = hp;
        StartCoroutine(nameof(DisableHPbar));
    }
    public void HandleEnemyDead(int a)
    {
        Destroy(gameObject);
    }
    IEnumerator DisableHPbar()
    {
        yield return new WaitForSeconds(1f);
        fill.enabled = false;
        border.enabled = false;
    }
}
