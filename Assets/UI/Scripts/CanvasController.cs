using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CanvasController : MonoBehaviour
{
    public UnityEvent<Canvas_State> OnStateChange;
    Canvas_State canvasState;
    public Canvas_State CanvasState
    {
        get => canvasState;
        set
        {
            canvasState = value;
            OnStateChange.Invoke(value);
        }
    }
    public GameObject[] Canvas;
   

    void Start()
    {
        StateController(Canvas_State.MainMenu);
        OnStateChange.AddListener(StateController);
    }
    public void Rename()
    {
        CanvasState = Canvas_State.NameCard;
    }
    public void ShowVillage()
    {
       CanvasState = Canvas_State.Village;
    }
    public void ShowNameCard()
    {
        if (GameManager.instanceProfile.playerName == "")
        {
            CanvasState = Canvas_State.NameCard;
        }
        else
        {
            ShowVillage();
        }
    }
    public void ShowMainMenu()
    {
        CanvasState = Canvas_State.MainMenu;
    }
    public void ShowSetting()
    {
        CanvasState = Canvas_State.Option;
    }
    public void ShowStart()
    {
        CanvasState = Canvas_State.Start;
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void StateController(Canvas_State canvasState)
    {
        
        for (int i = 0; i < Canvas.Length; i++)
        {
            if (i == (int)canvasState)
            {
                Canvas[i].SetActive(true);
            }
            else
            {
                Canvas[i].SetActive(false);
            }
                
        }

    }
     
  public enum Canvas_State
    {
        MainMenu,
        Option,
        Start,
        NameCard,
        Village
    }
}
