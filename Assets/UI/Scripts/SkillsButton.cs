using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillsButton : MonoBehaviour
{
    public TextMeshProUGUI HpPoinUI;
    public TextMeshProUGUI MpPoinUI;
    public Image[] skillSlot;
    public Sprite lockIcon;
    private Attack attack;
    private void Start()
    {
         attack = FindObjectOfType<Attack>();

        UpdateSkillIcon(1);
        attack.OnSkillChange.AddListener(ChangeSkillUI);
        GameManager.instanceProfile.OnSkillUnlockChange.AddListener(UpdateSkillIcon);
        ChangeHpPotionUI(GameManager.instanceProfile.HpPotion);
        ChangeMpPotionUI(GameManager.instanceProfile.MpPotion);
        GameManager.instanceProfile.OnHpPotionUse.AddListener(ChangeHpPotionUI);
        GameManager.instanceProfile.OnHpPotionChange.AddListener(ChangeHpPotionUI);
        GameManager.instanceProfile.OnMpPotionUse.AddListener(ChangeMpPotionUI);
        GameManager.instanceProfile.OnMpPotionChange.AddListener(ChangeMpPotionUI);
    }
    private void UpdateSkillIcon(int value)
    {
        for (int i = 0; i < attack.Skills.Length; i++)
        {

            skillSlot[i].sprite = attack.Skills[i].icon;
        }
        for (int i = attack.Skills.Length - 1; i >= GameManager.instanceProfile.SkillUnlock; i--)
        {
            skillSlot[i].sprite = lockIcon;
        }
    }
    public void ChangeSkillUI(int index)
    {
        for (int i = 0; i < skillSlot.Length; i++)
        {
            skillSlot[i].gameObject.transform.localScale = new Vector3(1, 1, 1);
        }
        skillSlot[index].gameObject.transform.localScale = new Vector3(1.2f, 1.2f, 1);
    }

    public void ChangeHpPotionUI(int value)
    {
        HpPoinUI.text = value.ToString(); 
    }
    public void ChangeMpPotionUI(int value)
    {
        MpPoinUI.text = value.ToString();
    }
}
