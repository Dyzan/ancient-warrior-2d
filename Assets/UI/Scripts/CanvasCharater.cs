using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCharater : MonoBehaviour
{
   public void ResumeGame()
    {
        Time.timeScale = 1;
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
}
