using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioOptionsManager : MonoBehaviour
{
    public static float musicVolume { get; private set; }
    public static float SFXVolume { get; private set; }

    public static void OnMusicSliderValueChange(float value)
    {
        musicVolume = value;
        SoundManager.instaceSound.UpdateMixerVolume();
    }
    public static void OnSFXSliderValueChange(float value)
    {
        SFXVolume = value;
        SoundManager.instaceSound.UpdateMixerVolume();
    }
}
