using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    public Slider _slider;
    public bool music;
    public bool sFX;
    
    private void Start()
    {
        
        if (music)
        {
            _slider.value = AudioOptionsManager.musicVolume;
            _slider.onValueChanged.AddListener(val => AudioOptionsManager.OnMusicSliderValueChange(val));
        }
        else if(sFX)
        {
            _slider.value = AudioOptionsManager.SFXVolume;
            _slider.onValueChanged.AddListener(val => AudioOptionsManager.OnSFXSliderValueChange(val));
        }
        
    }
}
