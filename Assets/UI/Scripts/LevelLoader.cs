using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public GameObject fade;
    public int levelToLoad;

    private void Start()
    {
        fade.SetActive(true);
    }
    public void FadeToLevel(int levelIndex)
    {
        levelToLoad = levelIndex;
        transition.SetTrigger("FadeOut");
        SoundManager.instaceSound.UpdateMusic(levelIndex);
    }
    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }

}
