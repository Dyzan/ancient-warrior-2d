using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InputPlayerName : MonoBehaviour
{
    public TMP_InputField inputField;
    public GameObject hideButton;

    private void Update()
    {
        UpdateShowButton();
    }
    public void SaveName()
    {
        GameManager.instanceProfile.SavePlayerName(inputField.text);
    }
    
    public void UpdateShowButton()
    {
        if (inputField.text.Length >= 3)
        {
            hideButton.SetActive(false);
        }
        else
        {
            hideButton.SetActive(true);
        }
    }
}
