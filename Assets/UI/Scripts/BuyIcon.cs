using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BuyIcon : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI price;
    [SerializeField] public Image logo;
    [SerializeField] public Button button;
    [SerializeField] public int skillID;
    public UnityEvent<int> OnClick;
}
