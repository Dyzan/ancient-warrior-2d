using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasLoseGame : MonoBehaviour
{
    public void LoseGame()
    {
        gameObject.SetActive(true);
    }
    public void RestartLevel()
    {
        GameManager.instanceProfile.SavePlayerPro5();
        var currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene);
        SoundManager.instaceSound.UpdateMusic(currentScene);
    }
    public void BackToMainMenu()
    {
        GameManager.instanceProfile.SavePlayerPro5();
        SceneManager.LoadScene(0);
        SoundManager.instaceSound.UpdateMusic(0);
    }
}
