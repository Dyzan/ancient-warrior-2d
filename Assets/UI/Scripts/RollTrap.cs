using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollTrap : MonoBehaviour
{
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<PlayerController>().CurrentHP = 0;
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
