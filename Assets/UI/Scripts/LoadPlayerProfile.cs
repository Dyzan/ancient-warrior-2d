using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadPlayerProfile : MonoBehaviour
{
    public TMP_Text playerName;
    public TMP_Text highScore;
    [SerializeField] SliderBar Hpbar;
    [SerializeField] SliderBar Mpbar;
   

    public void Start()
    {
        playerName.text = GameManager.instanceProfile.playerName;
        highScore.text = GameManager.instanceProfile.highScore.ToString();
        StartCoroutine(nameof(TryGetPlayerInfor));
        GameManager.instanceProfile.OnCoinValueChange.AddListener(UpdateCoin);
        UpdateCoin(GameManager.instanceProfile.Coin);
    }
    private void UpdateCoin(int coin)
    {
        highScore.text = coin.ToString();
    }
    IEnumerator TryGetPlayerInfor()
    {
        yield return new WaitForEndOfFrame();
        var player = FindObjectOfType<PlayerController>();
        player.OnHPChange.AddListener(Hpbar.SetValue);
        player.OnMPChange.AddListener(Mpbar.SetValue);
        GameManager.instanceProfile.OnCoinValueChange.AddListener(UpdateCoin);

    }

}
