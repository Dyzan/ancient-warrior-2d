using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public GameObject OnwinCanvas;
    public GameObject OnloseCanvas;

    public static GameManager instanceProfile;
    #region playerSave infor
    public string playerName;
    public int highScore;
    private int coin;
    private int skillUnlock;
    private int mapUnlock;
    private int hpPotion;
    private int mpPotion;
    #endregion
    public int SkillUnlock
    {
        get { return skillUnlock; }
        set
        {
            skillUnlock = value;
            Debug.Log("Skill unlock no " + value);
            OnSkillUnlockChange.Invoke(value);
        }
    }
    public int MapUnlock
    {
        get { return mapUnlock; }
        set
        {
            mapUnlock = value;
            OnMapUnlockChange.Invoke(value);
        }
    }
    public int Coin
    {
        get => coin;
        set
        {
            coin = value;
            OnCoinValueChange.Invoke(value);
        }

    }
    public int HpPotion
    {
        get { return hpPotion; }
        set
        {
            if (value < hpPotion)
            {
                OnHpPotionUse.Invoke(value);
            }
            else
            {
                OnHpPotionChange.Invoke(value);
            }
            hpPotion = value;

        }
    }
    public int MpPotion
    {
        get { return mpPotion; }
        set
        {
            if (value < mpPotion)
            {
                OnMpPotionUse.Invoke(value);
            }
            else
            {
                OnMpPotionChange.Invoke(value);
            }
            mpPotion = value;


        }
    }

    public UnityEvent<int> OnCoinValueChange;
    public UnityEvent<int> OnSkillUnlockChange, OnMapUnlockChange, OnHpPotionUse, OnMpPotionUse, OnHpPotionChange, OnMpPotionChange;

    public int HpValue, MpValue;
    private void OnEnable()
    {
        if (instanceProfile == null)
        {
            instanceProfile = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);
        MapUnlock = 1;
        SkillUnlock = 1;
        HpPotion = 5;
        MpPotion = 5;
        LoadPlayerPro5();

    }
    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        Time.timeScale = 1;
    }

    public void LoadPlayerPro5()
    {
        string pathWay = Application.persistentDataPath + "/dataMRA.txt";
        Debug.Log(pathWay);
        if (File.Exists(pathWay))
        {
            string json = File.ReadAllText(pathWay);
            SaveFile playerInfor = JsonUtility.FromJson<SaveFile>(json);
            instanceProfile.playerName = playerInfor.playerName;
            instanceProfile.highScore = playerInfor.highScore;
            instanceProfile.Coin = playerInfor.coin;
            instanceProfile.HpPotion = playerInfor.hpPotion;
            instanceProfile.MpPotion = playerInfor.mpPotion;
            instanceProfile.SkillUnlock = playerInfor.skillUnlock;
            instanceProfile.MapUnlock = playerInfor.mapUnlock;
        }
    }
    public void SavePlayerPro5()
    {
        string pathWay = Application.persistentDataPath + "/dataMRA.txt";
        SaveFile playerInfor = new SaveFile();
        playerInfor.playerName = GameManager.instanceProfile.playerName;
        playerInfor.highScore = GameManager.instanceProfile.highScore;
        playerInfor.coin = GameManager.instanceProfile.coin;
        playerInfor.mapUnlock = GameManager.instanceProfile.mapUnlock;
        playerInfor.hpPotion = GameManager.instanceProfile.hpPotion;
        playerInfor.mpPotion = GameManager.instanceProfile.mpPotion;
        playerInfor.skillUnlock = GameManager.instanceProfile.skillUnlock;
        string json = JsonUtility.ToJson(playerInfor);
        File.WriteAllText(pathWay, json);
        Debug.Log(pathWay);
    }
    public void SavePlayerName(string name)
    {
        playerName = name;
    }
    public void LoadPlayerName(string name)
    {
        name = playerName;
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void WinGame()
    {
        Instantiate(OnwinCanvas);
    }
    public void LoseGame()
    {
        Instantiate(OnloseCanvas);
    }
}
public class SaveFile
{
    #region playerName
    public string playerName;
    public int highScore;
    public int coin;
    public int skillUnlock;
    public int mapUnlock;
    public int hpPotion;
    public int mpPotion;
    #endregion
}
