using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasWinGame : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI CoinText; 
    public void WinGame()
    {
      gameObject.SetActive(true);
        CoinText.text = FindObjectOfType<MapHandle>().coinExtra.ToString();

    }
    public void LoseGame()
    {
        gameObject.SetActive(true);
    }
    public void LoadNextLevel()
    {
        GameManager.instanceProfile.SavePlayerPro5();
        var sceneIndex = SceneManager.GetActiveScene().buildIndex;
        var maxScene = SceneManager.sceneCountInBuildSettings;
        Debug.Log(sceneIndex);
        Debug.Log(maxScene);
        if (sceneIndex < maxScene - 1)
        {
            SceneManager.LoadScene(sceneIndex + 1);
            SoundManager.instaceSound.UpdateMusic(sceneIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(0);
            SoundManager.instaceSound.UpdateMusic(0);
        }
    } 
    public void BackToMainMenu()
    {
        GameManager.instanceProfile.SavePlayerPro5();
        SceneManager.LoadScene(0);
        SoundManager.instaceSound.UpdateMusic(0);
    }
    
}
