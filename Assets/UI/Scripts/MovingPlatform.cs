using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform beginPoint;
    public Transform endPoint;
    public float speed = 2f;
    public float smoothTime = 0.1f;

    public bool isForwarding;
    private Vector2 velocity;

    private void FixedUpdate()
    {
        MoveForward();
    }
    private void MoveForward()
    {
        var destination = isForwarding ? (Vector2)endPoint.position : (Vector2)beginPoint.position;
        transform.position = Vector2.SmoothDamp((Vector2)transform.position, destination,ref velocity, smoothTime, speed, Time.deltaTime);
        //transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
        if ((Vector2)transform.position == destination)
        {
            isForwarding = !isForwarding;
        }
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(beginPoint.position, endPoint.position);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
            collision.transform.parent = this.transform;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
            collision.transform.parent = null;
    }
}