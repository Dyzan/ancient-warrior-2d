using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyButton : MonoBehaviour
{
    [SerializeField] private GameObject buyCanvas;
    [SerializeField] private GameObject HpBuy;
    [SerializeField] private GameObject MpBuy;
    [SerializeField] private GameObject baseCanvas;
    [SerializeField] private List<GameObject> SkillBuy;
    [SerializeField] private GameObject SkillPanel;
    public void OnBuyClick()
    {

        ClearSkillBuy();
        if (buyCanvas.activeInHierarchy)
        {

            buyCanvas?.SetActive(false);
        }
        else
        {
            buyCanvas.SetActive(true);
        }

    }
    public void OnClose()
    {
        buyCanvas?.SetActive(false);

        ClearSkillBuy();
    }
    public void OnPotionCanvasClick()
    {

        ClearSkillBuy();
        HpBuy.SetActive(true);
        MpBuy.SetActive(true);
    }
    public void OnSkillCanvasClick()
    {
        ClearSkillBuy();
        var attack = FindObjectOfType<Attack>();
        MpBuy?.SetActive(false);
        HpBuy?.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            var skillBuy = Instantiate(baseCanvas);
            var buyIcon = skillBuy.GetComponent<BuyIcon>();
            buyIcon.logo.sprite = attack.Skills[i].icon;
            buyIcon.skillID = i + 1;

            if (GameManager.instanceProfile.SkillUnlock > i)
            {
                buyIcon.button.gameObject.SetActive(false);
                buyIcon.price.gameObject.SetActive(false);


            }
            else
            {
                var price = attack.Skills[i].price;
                buyIcon.price.text = price.ToString() + "G";
                buyIcon.button.onClick.AddListener(() => OnSkillBuy(buyIcon.skillID, price));
            }
            buyIcon.transform.parent = SkillPanel.transform;
            SkillBuy.Add(skillBuy);
        }
    }
    public void OnHpPotionBuy()
    {
        if ((GameManager.instanceProfile.Coin - 5) >= 0)
        {
            GameManager.instanceProfile.Coin -= 5;
            GameManager.instanceProfile.HpPotion++;
        }
    }
    public void OnMpPotionBuy()
    {
        if ((GameManager.instanceProfile.Coin - 10) >= 0)
        {
            GameManager.instanceProfile.Coin -= 10;
            GameManager.instanceProfile.MpPotion++;
        }
    }
    public void OnSkillBuy(int id,int price)
    {
        Debug.Log("Skill prite: " + price);
        if(GameManager.instanceProfile.Coin > price)
        {
            GameManager.instanceProfile.SkillUnlock = id;
            GameManager.instanceProfile.Coin -= price;
            OnSkillCanvasClick();
        }
    
    }
    private void ClearSkillBuy()
    {
        foreach (var x in SkillBuy)
        {
            Destroy(x);
        }
    }
}
