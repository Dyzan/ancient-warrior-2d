using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRolling : MonoBehaviour
{
    public GameObject rollRockPref;
    public float respawnTime = 1.0f;

    private void Start()
    {
        StartCoroutine(Wave());
    }
    private void SpawnGO()
    {
        GameObject a = Instantiate(rollRockPref) as GameObject;
        a.transform.position = transform.position;
        a.transform.rotation = Quaternion.identity;
    }
    IEnumerator Wave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            SpawnGO();
        }
        
    }
}
