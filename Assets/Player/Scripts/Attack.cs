using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class Attack : MonoBehaviour
{

    [SerializeField] Transform FirePoint;
    [SerializeField] float attackRate = 2f;
    [SerializeField] float posFire;
    [SerializeField] float nextAttackTime = 0f;
    [SerializeField] PlayerController playerController;
    public UnityEvent<float> attackAngle;
    public GameObject Bullet;
    public PlayerSkill[] Skills;
    public GameObject Uitext;
    private int currentSkill;
    private int skillUnlock;
    public int CurrentSkill
    {
        get => currentSkill;
        set
        {
            currentSkill = value;
            OnSkillChange.Invoke(value);
            currentSkillObject = Skills[value];
        }
    }
    private PlayerSkill currentSkillObject;
    public UnityEvent<int> OnSkillChange;
    Animator animator;


    void Start()
    {

        playerController = GetComponent<PlayerController>();
        CurrentSkill = 0;
        animator = GetComponent<Animator>();
        GameManager.instanceProfile.OnSkillUnlockChange.AddListener(UpdateSkill);
        UpdateSkill(GameManager.instanceProfile.SkillUnlock);
    }
    void UpdateSkill(int value)
    {
        skillUnlock = value;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            CurrentSkill = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Debug.Log(skillUnlock);
            if(skillUnlock >=2) CurrentSkill = 1;

        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (skillUnlock >= 3) CurrentSkill = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (skillUnlock >= 4) CurrentSkill = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (skillUnlock >= 5) CurrentSkill = 4;
        }
        if (Time.time >= nextAttackTime)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                animator.SetTrigger("Attack");
                Vector3 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector3 trueDir = dir - transform.position;
                Shoot(trueDir.normalized, Bullet);
                nextAttackTime = Time.time + 0.5f / attackRate;
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                //   animator.SetTrigger("Attack");
               
                if ((playerController.CurrentMP -= currentSkillObject.mpCost) >= 0)
                {
                    Skills[CurrentSkill].OnSkillActive(this);
                    nextAttackTime = Time.time + 0.5f / attackRate;
                }
                else
                {
                   
                    Uitext.GetComponentInChildren<TextMeshProUGUI>().text = "Not Enouch MP to Cast";
                  var text =   Instantiate(Uitext);
                    text.transform.localPosition = this.transform.localPosition;
                }

            }
        }

        if(Input.GetKeyDown(KeyCode.E))
        {
            if(GameManager.instanceProfile.HpPotion >0)
            {
                GameManager.instanceProfile.HpPotion--;
            }
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            if (GameManager.instanceProfile.MpPotion > 0)
            {
                GameManager.instanceProfile.MpPotion--;
            }
        }
    }
    public void Shoot(Vector3 dir, GameObject skill)
    {
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        GameObject bullet = Instantiate(skill);
        bullet.transform.position = new Vector3(FirePoint.position.x, FirePoint.position.y + posFire, 1);
        bullet.transform.rotation = Quaternion.Euler(0, 0, angle);

        if (angle > -90 && angle < 90)
            bullet.transform.localScale = new Vector3(1, 1, 1);
        else
            bullet.transform.localScale = new Vector3(1, -1, 1);
        //  Use UnityEvent notification to any scripts have Addlistener method 
        attackAngle.Invoke(angle);
    }
}
