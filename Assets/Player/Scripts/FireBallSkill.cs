using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="SKILL/ShotSkill")]
public class FireBallSkill : PlayerSkill
{
    public GameObject skillPerfabs;
    public override void OnSkillActive(Attack attack)
    {
        base.OnSkillActive(attack);
        Vector3 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 trueDir = dir - attack.transform.position;
        attack.Shoot(trueDir.normalized, skillPerfabs);
    }
}
