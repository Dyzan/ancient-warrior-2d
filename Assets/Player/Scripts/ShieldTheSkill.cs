using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SKILL/ShieldSkill")]
public class ShieldTheSkill : PlayerSkill
{
    public Shield shieldObject;
    public int blockTime;
    public float activeTime;
    public override void OnSkillActive(Attack attack)
    {
        base.OnSkillActive(attack);
        var oj = Instantiate(shieldObject);
        oj.transform.localPosition = attack.transform.position;
        oj.transform.parent = attack.transform;
        oj.blockValue = blockTime;
        oj.CallDestroyCountDown(activeTime);
    }
}
