using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    #region HP and MP
    [SerializeField] int maxHP;
    int currentHP;
    public int CurrentHP
    {
        get => currentHP;
        set
        {

            if (value > maxHP)
            {
                currentHP = maxMP;
            }
            else if (value < 0)
            {
                currentHP = 0;
            }
            else

            {
                currentHP = value;
            }
            OnHPChange.Invoke(currentHP, maxHP);
        }
    }
    [SerializeField] int maxMP;
    int currentMP;
    public int CurrentMP
    {
        get => currentMP;
        set
        {
            if (value > maxMP)
            {
                currentMP = maxMP;
            }
            else if (value < 0)
            {
                currentMP = 0;
            }
            else

            {
                currentMP = value;
            }

            OnMPChange.Invoke(currentMP, maxMP);
        }
    }
    #endregion
    Rigidbody2D rb;
    Animator animator;
    [SerializeField] public Transform groundCheckCollider;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float JumpPower;
    //[SerializeField] int totalJumps;
    #region event
    public UnityEvent<int, int> OnHPChange, OnMPChange;
    #endregion

    public float speed;
    public float jumpRate = 2f;
    float nextJumpTime = 0f;
    float getDameRadius = 5f;

    const float groundCheckRadius = 0.2f;
    float runSpeedModifier = 2f;
    float HorizontalValue;
    bool isGrounded;
    public bool IsGrounded
    {
        get => isGrounded;
        set
        {
            isGrounded = value;
            if (value == true)
            {
                var col = Physics2D.Raycast(this.transform.position, Vector2.down, 5f);
                transform.SetParent(col.transform, true);
            }
            else
            {
                transform.SetParent(null);
            }
        }

    }
    bool facingRight = true;
    bool isRunning;
    //int availableJumps;
    //bool multipleJums;


    public void Start()
    {
        CurrentHP = maxHP;
        CurrentMP = maxMP;
        GameManager.instanceProfile.OnHpPotionUse.AddListener(OnHpPotionUse);
        GameManager.instanceProfile.OnMpPotionUse.AddListener(OnMpPotionUse);
        //availableJumps = totalJumps;
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        //Unity Event Check all file, if file have script Attack and not null => call method attackAngle and require function FlipByAttack run
        FindObjectOfType<Attack>()?.attackAngle.AddListener(FlipByAttack);
        StartCoroutine(MpRerent());
    }
    void Update()
    {
        //set the VelocityY for animation jumping and falling
        animator.SetFloat("VelocityY", rb.velocity.y);
        //Set the value for X
        HorizontalValue = Input.GetAxisRaw("Horizontal");
        //Debug.Log(HorizontalValue.ToString());
        if (Input.GetKeyDown(KeyCode.LeftShift))
            isRunning = true;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            isRunning = false;
        //if we press the button Jump
        if (Time.time >= nextJumpTime)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
                nextJumpTime = Time.time + 1f / jumpRate;
            }
        }


    }
    private void FixedUpdate()
    {
        Move(HorizontalValue);
        GroundCheck();
    }
    void GroundCheck()
    {
        bool wasGrounded = isGrounded;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheckCollider.position, groundCheckRadius, groundLayer);

        if (colliders.Length > 0)
        {
            IsGrounded = true;

        }
        else
        {
            IsGrounded = false;
        }



        wasGrounded = false;
        //when jumping will get "jumping" animator and we need to cancel that state when we land
        animator.SetBool("IsGround", isGrounded);
        animator.SetBool("Jump", false);
    }
    void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = Vector2.up * JumpPower;
            animator.SetTrigger("isJump");
        }
    }

    void Move(float dir)
    {
        #region Move & Run
        //set value of moveSpeed using direction and speed
        float moveSpeed = dir * speed * 100 * Time.fixedDeltaTime;
        //If we are running => running modifier
        if (isRunning)
            moveSpeed *= runSpeedModifier;
        //Create Vector2 for the velocity
        Vector2 targetVelocity = new Vector2(moveSpeed, rb.velocity.y);
        //set the player's Velocity
        rb.velocity = targetVelocity;

        //Rotate direction of player
        if (facingRight && dir < 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, 0);
            facingRight = false;
        }
        else if (!facingRight && dir > 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, 0);
            facingRight = true;
        }

        //idle = 0 , walk = 6 , run = 12
        //set the xVelocity of player base on the x value
        //of the RigitBody2D velocity
        animator.SetFloat("VelocityX", Mathf.Abs(rb.velocity.x));
        #endregion
    }

    private void FlipByAttack(float angle)
    {
        if (angle > -90 && angle < 90)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 0);
            facingRight = true;
        }
        else
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y, 0);
            facingRight = false;
        }
    }
    IEnumerator MpRerent()
    {
        yield return new WaitForSeconds(2f);
        CurrentMP++;
        StartCoroutine(MpRerent());
    }
    private void OnHpPotionUse(int value)
    {
        CurrentHP += GameManager.instanceProfile.HpValue;
    }
    private void OnMpPotionUse(int value)
    {
        CurrentMP += GameManager.instanceProfile.MpValue;
    }
    public void GetHurt()
    {
        animator.SetTrigger("getHurt");
    }

    public void Die()
    {
        animator.SetTrigger("Die");

    }
}
