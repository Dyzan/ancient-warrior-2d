using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public float blockValue = 1;
    public Transform target;
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy_weapon"))
        {
            blockValue--;
            Destroy(collision.gameObject);
            if (blockValue < 0)
                Destroy(gameObject);
        }
    }
    private void Update()
    {
       // transform.position = target.position;
    }
    public void CallDestroyCountDown(float time)
    {
        StartCoroutine(DestroyTime(time));
    }
   private IEnumerator DestroyTime(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
