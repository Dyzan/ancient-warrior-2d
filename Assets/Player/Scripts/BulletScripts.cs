using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScripts : MonoBehaviour
{
    public float maxRange;
    public Vector3 startPos;
    public bool isAOE;
    public GameObject ExplosionEffect;
    public float speed;
    public int dame;
    public int mpCost;
    public Sprite SkillIcon;
    private void Start()
    {
        startPos = transform.position;
    }
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        //If bullet has max range It will destroy 
        if ((transform.position - startPos).magnitude > maxRange)
            Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Ground"))
        {
            Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
            if (isAOE)
            {
                var explos = Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
                explos.GetComponent<Explosion>().DealDame();

            }
            else
            {
                if (collision.TryGetComponent<EnemyAI>(out var enemyAI))
                    enemyAI.GetHit(dame);
                if (collision.TryGetComponent<Boss>(out var boss))
                { boss.HP -= (dame);
                    Debug.Log(dame);
                }    
                   
            }
        }



    }
}

