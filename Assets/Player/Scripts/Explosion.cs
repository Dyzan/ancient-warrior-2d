using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float AoERange;
    public int dame;
    public int AoENumber;
    public void DealDame()
    {
        int numberOfAOE = 0;
        var arr = Physics2D.OverlapCircleAll(this.transform.position, AoERange);
        foreach (var collider in arr)
        {
            if (collider.CompareTag("Enemy") && numberOfAOE < AoENumber)
            {
              
                if (collider.TryGetComponent<EnemyAI>(out var enemyAI))
                    enemyAI.GetHit(dame);
                if (collider.TryGetComponent<Boss>(out var boss))
                {
                    boss.HP -= (dame);
                    Debug.Log("Boss an dame");
                }
                    
                numberOfAOE++;
            }

        }
    }
    public void DestroyRightAway()
    {
        Destroy(this.gameObject);
    }
}
