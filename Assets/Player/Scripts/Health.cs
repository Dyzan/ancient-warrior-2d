using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    private Animator animation;


    private void Start()
    {
        animation = GetComponent<Animator>();
    }

    private void GetHurt()
    {
        animation.SetTrigger("getHurt");
    }
    private void PlayerDie()
    {
         animation.SetTrigger("isDie");
        //Do something here ! 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy_weapon") || collision.CompareTag("Enemy"))
            GetHurt();
    }
}
