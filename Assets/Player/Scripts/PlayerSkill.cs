using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill : ScriptableObject
{
    public int mpCost;
    public Sprite icon;
    public AudioClip clip;
    public GameObject SkillEffect;
    public int price;
    public virtual void OnSkillActive(Attack attack)
    {
        if(clip != null)
        {
            attack.GetComponent<AudioSource>().clip = clip;
            attack.GetComponent<AudioSource>().Play();
        }
        if(SkillEffect != null)
        {
           var skill =  Instantiate(SkillEffect,attack.transform);
            skill.transform.localPosition = new Vector3(0, 0, 0);
        
           
        }
     
    }
}
