using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SKILL/Teleport")]
public class TeleportSkill : PlayerSkill
{
    public float distance;
    public override void OnSkillActive(Attack attack)
    {
        var size = attack.GetComponent<CapsuleCollider2D>().size;
        float baseDis = distance;
        base.OnSkillActive(attack);
        float dis;
        bool stillLoop = true;
        float x, y;
        do
        {
            baseDis -= 1f;
            if (attack.transform.localScale.x > 0)
            {
                dis = baseDis;
            }
            else
            {
                dis = -baseDis;
            }
            x = attack.transform.position.x + dis;
            y = attack.transform.position.y;
            Vector3 newPos = new Vector3(x, y, 0);
            var cols = Physics2D.OverlapCapsuleAll(newPos, size, CapsuleDirection2D.Vertical, 0f);
            Debug.Log(cols.Length);
            if (cols.Length==0)
            {
                stillLoop = false;

            }

         

            if (baseDis <= 0)
            {  
                stillLoop = false;
            }


        } while (stillLoop);
        attack.transform.parent = null;
        Debug.Log(attack.transform.position);
        Debug.Log(new Vector3(x, y, attack.transform.position.z));
        attack.transform.position = new Vector3(x, y, attack.transform.position.z);
    }
    //private Vector3 AbleDistance(float dis, Attack attack)
    //{
    //    var collider = attack.GetComponent<CapsuleCollider2D>();
    //    var colliderSize = collider.size;
    //    float x, y;
    //    do
    //    {


    //        var col = Physics2D.Raycast(attack.transform.position, Vector2.down, 5f);
    //        var col2 = Physics2D.Raycast(new Vector2(x, attack.transform.position.y), Vector2.down, 5f);
    //        if (col2.collider != null)
    //        {
    //            var yOffset = col2.transform.position.y - col.transform.position.y;
    //            y = attack.transform.position.y + yOffset;

    //        }
    //        else
    //        {
    //            y = attack.transform.position.y;
    //        }
    //        dis -= 0.5f;
    //    }


    //    return new Vector2(x, y);
    //}

}
